import React, {useState, useEffect} from 'react'
import axios from 'axios'

function DataFetching() {
    // const URL = `https://jsonplaceholder.typicode.com/posts/${id}`
    const [post, setPost] =  useState({})
    const [id, setId] = useState(1)
    const [idFromButtonClick, setIdFromButtonClick] = useState(1)


    useEffect(() => {
        axios.get(`https://jsonplaceholder.typicode.com/posts/${idFromButtonClick}`)
        .then(res => {
            console.log(res)
            setPost(res.data)
            console.log(id)
        })
        .catch(err => {
            console.log(err)
        })
    }, [idFromButtonClick])

    const handleClick = e => {
        setIdFromButtonClick(id)
    }
    return (
        <div>
            <input type="text" value={id} onChange={e => setId(e.target.value)}/>
            <button type="button" onClick={handleClick}>Fetch Data</button>
            {/* <ul>
                {
                    posts.map(post => (
                        <li key={post.id}>{post.title}</li>
                    ))
                }
            </ul> */}
            <div>{post.title}</div>
        </div>
    )
}

export default DataFetching
