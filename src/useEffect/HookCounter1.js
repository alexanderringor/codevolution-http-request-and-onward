import React, {useState, useEffect} from 'react'

function HookCounter1() {
    const [count, setCount] = useState(0)
    const [name, setName] = useState('')
    useEffect(() => {
        document.title = `You clicked ${count} times`
        console.log('test')
    }, [count])

    return (
        <div>
            <input type="text" value={name} onChange={e => setName(e.target.value)}></input>
            <button onClick={() => setCount(count +1)}>Click {count} </button>
        </div>
    )
}

export default HookCounter1
