import React, { Component } from 'react'
import axios from 'axios'
// import {Button} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';

class PostForm extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             userId: '',
             title: '',
             body: '',
             posts: []
        }
    }

    changeHandler = e => {
        this.setState({[e.target.name]: e.target.value})
    }

    onSubmitHandler = e => {
        e.preventDefault()
        console.log(this.state)

        axios.post('https://jsonplaceholder.typicode.com/posts', this.state)
        .then(response => {
            this.setState({
                posts: response.data
            })
        })
        .catch(error => {
            console.log(error)
        })

    }
    
    
    render() {
        const { userId, title, body, posts} = this.state

        return (
            <div>
                <form onSubmit={this.onSubmitHandler}>
                    <div>
                        <input type="text" name="userId" value={userId} onChange={this.changeHandler}/>
                    </div>
                    <div>
                        <input type="text" name="title" value={title} onChange={this.changeHandler}/>
                    </div>
                    <div>
                        <input type="text" name="body" value={body} onChange={this.changeHandler}/>
                    </div>

                    <h3>Post List</h3>
                    {
                        posts ?
                        <div>{posts.title}</div> : null
                    }
                    <button className="btn btn-danger">Submit</button>
                    {/* <Button>Test</Button> */}
                    
                </form>
            </div>
        )
    }
}

export default PostForm
