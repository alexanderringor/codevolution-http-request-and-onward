import React, { useState } from 'react'

function HookCounter() {

        // array destructuring
   const[count, setCount] = useState(0)

    return (
        <div>
            <h1>State Hooks</h1>
<button onClick={() => setCount(count + 1)}>Click {count}</button>
        </div>
    )
}

export default HookCounter
