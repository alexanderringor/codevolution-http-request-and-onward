import React from 'react';
// import ComponentC from './useContext/ComponentC';
import Counter1 from './useReducer/Counter1';
import CounterTwo from './useReducer/CounterTwo';
// import DataFetching from './useEffect/DataFetching';
// import ClassCounter from './Components/ClassCounter';
// import HookCounter from './Components/HookCounter';
// import HookCounter2 from './Components/HookCounter2';
// import HookCounter3 from './Components/HookCounter3';
// import HookCounter4 from './Components/HookCounter4';
// import PostList from './Components/PostList'
// import PostForm from './Components/PostForm';
// import HookCounter1 from './useEffect/HookCounter1'
// import HookMouse from './useEffect/HookMouse';
// import MouseContainer from './useEffect/MouseContainer';
// import IntervalHookCounter from './useEffect/IntervalHookCounter';

// export const UserContext = React.createContext()
// export const ChannelContext = React.createContext()



function App() {
  return (
    <div className="App">
      {/* <PostList /> */}
      {/* <PostForm /> */}
      {/* <ClassCounter /> */}
      {/* <HookCounter /> */}
      {/* <HookCounter2 /> */}
      {/* <HookCounter3 /> */}
      {/* <HookCounter4 /> */}
      {/* <HookCounter1 /> */}
      {/* <HookMouse /> */}
      {/* <MouseContainer /> */}
      {/* <IntervalHookCounter /> */}
      {/* <DataFetching />  */}
      {/* <UserContext.Provider value={'Alex'}>
        <ChannelContext.Provider value={'Ringor'}>
          <ComponentC />        
        </ChannelContext.Provider>
      </UserContext.Provider> */}
      {/* <Counter1 /> */}
      <CounterTwo />
    </div>
  );
}

export default App;
