import React, {useContext} from 'react'
import ComponentF from './ComponentF'
import {UserContext, ChannelContext} from '../App'

function ComponentE() {
    const context  = useContext(UserContext)
    const channel = useContext(ChannelContext)
    return (
        <div>
            {context} {channel}
        </div>
    )
}

export default ComponentE
